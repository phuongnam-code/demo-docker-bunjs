import figlet from "figlet";

const server = Bun.serve({
  port: Bun.env.PORT,
  fetch(req) {
    const body = figlet.textSync("hi, my bun application (prod)!");
    return new Response(body);
  },
});

console.log(`Listening on http://localhost:${server.port}`);
