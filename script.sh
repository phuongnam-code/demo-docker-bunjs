#!/bin/bash

# Move to repository
# cd /path/to/your/demo-docker-bunjs

# Switch to the specified branch (master or dev)
if [ "$1" = "master" ]; then
  git checkout master
elif [ "$1" = "dev" ]; then
  git checkout dev
else
  echo "Usage: $0 <branch_name>"
  exit 1
fi

# Check if the branch checkout was successful or not
if [ $? -eq 0 ]; then
  # Build Docker image from Dockerfile and run container
  if [ "$1" = "master" ]; then
    docker build -t demo-docker-bunjs .
    docker container run -d -e NODE_ENV=production -p 3000:3000 demo-docker-bunjs
  else
    docker build -t demo-docker-bunjs-dev .
    docker container run -d -e NODE_ENV=development -p 3001:3001 demo-docker-bunjs-dev
  fi
else
  echo "Error: Failed to checkout branch."
fi