#!/bin/bash

# Move to repository
# cd /path/to/your/demo-docker-bunjs

# Build Docker image from Dockerfile and run container
echo "---- production ----"
git checkout master
echo "Docker image starting"
docker build -t demo-docker-bunjs .
echo "Docker image done, starting container"
docker container run -d -e NODE_ENV=production -p 3000:3000 demo-docker-bunjs
echo "Docker container done"

echo "---- development ----"
git checkout dev
echo "Docker image starting"
docker build -t demo-docker-bunjs-dev .
echo "Docker image done, starting container"
docker container run -d -e NODE_ENV=development -p 3001:3001 demo-docker-bunjs-dev
echo "Docker container done"

echo "DONE"