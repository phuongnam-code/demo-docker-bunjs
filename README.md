# How to run?

#### Run normal:

To install dependencies:

```
bun install
```

To run:

```
bun run dev
```

#### Run with docker

Step 1:

```
docker build -t demo-docker-bunjs .
```

Step 2:

```
docker container run -d -e NODE_ENV=development -p 3001:3001 demo-docker-bunjs
```

#### Auto run

`sudo sh ./script.sh dev` or
`sudo sh ./script.sh master` or
`sudo sh ./script_both.sh`

# How to check it?

Open browser enter link [http://localhost:3000/]()

> This project was created using `bun init` in bun v1.0.1. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
