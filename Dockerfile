FROM oven/bun:1

  WORKDIR /usr/src/app

  COPY . .

  RUN bun install --silent

  ARG NODE_ENV=development

  ENV NODE_ENV $NODE_ENV
  ENV PRODUCTION_PORT=3000
  ENV DEVELOPMENT_PORT=3001

  RUN if [ "$NODE_ENV" = "production" ]; then \
      export PORT="$PRODUCTION_PORT"; \
      else \
        export PORT="$DEVELOPMENT_PORT"; \
      fi

  CMD ["bun", "run", "start:docker", "--env", "$NODE_ENV", "--port", "$PORT"]